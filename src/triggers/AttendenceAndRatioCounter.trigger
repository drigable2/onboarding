trigger AttendenceAndRatioCounter on Participation__c (after update, after insert) {
    Set<Id> courseIds = new Set<Id>();
    for(Participation__c participant : Trigger.new) {
        courseIds.add(participant.Course__c);
    }
    List<Course__c> courses = [SELECT Id, Succes_ratio__c, Enrolled_students__c,
                               (SELECT Id, Result__c FROM Participations__r) 
                               FROM Course__c WHERE Id IN :courseIds];
    System.debug('SIZE  ' + courses.size());
    System.debug('=========  ' + courses);
    for(Course__c course : courses) {
        course.Enrolled_students__c = course.Participations__r.size();
        Double passedStudent = 0;
        Double failedStudent = 0;
        for(Participation__c participation : course.Participations__r){
            if(participation.Result__c == 'Pass')
                passedStudent++;
            else if(participation.Result__c == 'Fail')
                failedStudent++;
        }
        if(passedStudent + failedStudent != 0)
            course.Succes_ratio__c = (passedStudent / (passedStudent + failedStudent)) * 100;
          
    }
    update courses;
}