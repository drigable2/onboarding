trigger LeadTrigger on Lead (before insert, after insert) {

    if(Trigger.isAfter){
        Map<Id, Lead> leads = Trigger.newMap;
        List<Lead> insertedLeads = [SELECT Id, LeadSource, createdDate FROM Lead WHERE Id IN :leads.keySet()];
        LeadTriggerHandler lth = new LeadTriggerHandler(insertedLeads); 
        lth.assignLeadToCampaign();
    }
    
    if(Trigger.isBefore){
        LeadTriggerHandler ltbh = new LeadTriggerHandler(Trigger.new);
        List<Campaign> campaigns = ltbh.getParentCampaignsWithoutChildren();
        List<String> parentCampaingNames = new List<String>();
        for(Campaign campaign : campaigns){
          		parentCampaingNames.add(campaign.Name);           
        }
        if(!parentCampaingNames.equals(ltbh.getLeadSources()))
            Trigger.new[0].addError('No has parent Campaign');
    }
    
}