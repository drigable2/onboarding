trigger ProductTableAccordWarehouse on Product_Table__c (before insert) {
	
    List<Product_Table__c> productTables = new List<Product_Table__c>();
    for(Product_Table__c productTable : Trigger.new){     
        productTables.add(productTable);
    }
    
    ProductTableAccordWarehouseHandler ptah = new ProductTableAccordWarehouseHandler();  
    ptah.defineWarehouse(productTables);
    
}