@isTest
public class StudentTest {
    
    static List<Course__c> courses = new List<Course__c>();
    static Set<String> completedCourses = new Set<String>();
    static List<Participation__c> testparticiprtions = new List<Participation__c>();
    static List<Participation__c> toBeEvaluated = new List<Participation__c>();
    static List<Participation__c> insufficientAttendance = new List<Participation__c>(); 
    static List<Participation__c> graded = new List<Participation__c>();
    static Map<String, List<Participation__c>> completedParticipations = new Map<String, List<Participation__c>>();
    static List<Participation__c> participations = new List<Participation__c>();
    static List<Participation__c> evaluations;
    static string selectedCourse;
    static final Integer requiredHoursToBeEvaluated = 10;
    static final Integer duration = 20;
    
    private static void clearData() {
        delete [SELECT Id FROM Participation__c];
        delete [SELECT Id FROM Course__c];
        delete [SELECT Id FROM Student__c];
    }
    
    
    private static void setup() {
        List<Participation__c> evaluations;
       
                
        Course__c course = new Course__c();
        
        course.Start_date__c = Date.newInstance(2018, 5, 14); 
        course.Duration__c = duration; 
        course.End_date__c = Date.newInstance(2018, 5, 25);  
        course.Name = 'Salesforce';
        course.Succes_ratio__c = 0;
        
        insert course;
        
        Student__c st1 = new Student__c();
        Student__c st2 = new Student__c();
        Student__c st3 = new Student__c();
        Student__c st4 = new Student__c();
        
        st1.Email__c = 'st1@a.by';
        st1.Name = 'Vasy1';
        st1.last_Name__c = 'Petrov1';
        st2.Email__c = 'st2@a.by';
        st2.Name = 'Vasy2';
        st2.last_Name__c = 'Petrov2';
        st3.Email__c = 'st3@a.by';
        st3.Name = 'Vasy3';
        st3.last_Name__c = 'Petrov3';
        st4.Email__c = 'st4@a.by';
        st4.Name = 'Vasy4';
        st4.last_Name__c = 'Petrov4';       
 
        insert st1;
        insert st2;
        insert st3;
        insert st4;
        
        Participation__c part1 = new Participation__c();
        Participation__c part2 = new Participation__c();
        Participation__c part3 = new Participation__c();
        Participation__c part4 = new Participation__c();
        
        part1.Hours_attended__c = 13;
        part1.Grade__c = '';
        part1.Result__c = null;
        
        part2.Hours_attended__c = 12;
        part2.Grade__c = '';
        part2.Result__c = null;
        
        part3.Hours_attended__c = 15;
        part3.Grade__c = '';
        part3.Result__c = null;
        
        part4.Hours_attended__c = 9;
        part4.Grade__c = '';
        part4.Result__c = null;
        
        part1.Student__c = st1.Id;
        part2.Student__c = st2.Id;
        part3.Student__c = st3.Id;
        part4.Student__c = st4.Id;
        
        part1.Course__c = course.Id;
        part2.Course__c = course.Id;
        part3.Course__c = course.Id;
        part4.Course__c = course.Id;

        participations.add(part1);
        participations.add(part2);
        participations.add(part3);
        participations.add(part4);
        
    
        
        if(evaluations == null || evaluations.isEmpty()){
        
            insert participations;
        
            evaluations = [SELECT Hours_attended__c, 
                                             Grade__c, 
                                             Result__c, 
                                             Student__r.Name,
                                             Student__r.last_Name__c, 
                                             Course__r.End_date__c, 
                                             Course__r.Duration__c, 
                                             Course__r.Name,
                                             Course__r.Succes_ratio__c
                                             FROM Participation__c
                                             ];
        }
            
    }
    
    @isTest
        static void testCollectParticipations(){
        setup();
        
        evaluations = [SELECT Hours_attended__c, 
                                             Grade__c, 
                                             Result__c, 
                                             Student__r.Name,
                                             Student__r.last_Name__c, 
                                             Course__r.End_date__c, 
                                             Course__r.Duration__c, 
                                             Course__r.Name,
                                             Course__r.Succes_ratio__c
                                             FROM Participation__c
                                             ];
           
        System.assertEquals(false, evaluations.isEmpty()); 
        System.assertEquals(4, evaluations.size());  
        System.assertEquals(Date.newInstance(2018, 5, 25), evaluations[0].Course__r.End_date__c);
        System.assertEquals(Date.newInstance(2018, 5, 24) < evaluations[0].Course__r.End_date__c, true);
        clearData();
    }
    
    
    @isTest
    static void testCollectCompletedParticipations1() {
        setup();
        evaluations = [SELECT Hours_attended__c, 
                                             Grade__c, 
                                             Result__c, 
                                             Student__r.Name,
                                             Student__r.last_Name__c, 
                                             Course__r.End_date__c, 
                                             Course__r.Duration__c, 
                                             Course__r.Name,
                                             Course__r.Succes_ratio__c
                                             FROM Participation__c
                                             ];
        
        System.assertEquals(false, evaluations.isEmpty());
        StudentsController sc = new StudentsController();
        sc.collectCompletedParticipations(evaluations);
        System.assertEquals('Salesforce', (new List<String>(sc.getCompletedParticipations().keySet()))[0]);
        System.assertEquals(false, sc.getCompletedParticipations().get('Salesforce').isEmpty());
        clearData();
    }
    
    @isTest
    static void testCollectCompletedParticipations2() {
     setup();
     evaluations = [SELECT Hours_attended__c, 
                                             Grade__c, 
                                             Result__c, 
                                             Student__r.Name,
                                             Student__r.last_Name__c, 
                                             Course__r.End_date__c, 
                                             Course__r.Duration__c, 
                                             Course__r.Name,
                                             Course__r.Succes_ratio__c
                                             FROM Participation__c
                                             WHERE Hours_attended__c >=: requiredHoursToBeEvaluated
                                             ];
        
        //StudentsController sc = new StudentsController();
        evaluations.get(0).Course__r.Name = 'Trigger';
       // update evaluations.Course__r;
        StudentsController sc = new StudentsController();
        sc.collectCompletedParticipations(evaluations);
        System.assertEquals(0, completedParticipations.keySet().size());   
        System.assertEquals('Salesforce', (new List<String>(sc.completedParticipations.keySet()))[0]);
        clearData();
    }
    
    @isTest 
    static void testSortEvaluation() {
      setup();  
      evaluations = [SELECT Hours_attended__c, 
                                             Grade__c, 
                                             Result__c, 
                                             Student__r.Name,
                                             Student__r.last_Name__c, 
                                             Course__r.End_date__c, 
                                             Course__r.Duration__c, 
                                             Course__r.Name,
                                             Course__r.Succes_ratio__c
                                             FROM Participation__c
                                             WHERE Hours_attended__c < : requiredHoursToBeEvaluated
                                             ];
        
        StudentsController sc = new studentsController();
        sc.sortEvaluation(evaluations);
        System.assertEquals(0, toBeEvaluated.size());
        System.assertEquals(0, insufficientAttendance.size());
        System.assertEquals(0, graded.size());
        clearData();
    }
    
    @isTest 
    static void calculateResult1(){
        setup();
        evaluations = [SELECT Hours_attended__c, 
                                             Grade__c, 
                                             Result__c, 
                                             Student__r.Name,
                                             Student__r.last_Name__c, 
                                             Course__r.End_date__c, 
                                             Course__r.Duration__c, 
                                             Course__r.Name,
                                             Course__r.Succes_ratio__c
                                             FROM Participation__c
                                             WHERE Hours_attended__c >=: requiredHoursToBeEvaluated
                                             ];

        
        
    System.debug('evaluations' + evaluations);
        evaluations.get(0).Grade__c = 'A';
        evaluations.get(0).Result__c = '';
        evaluations.get(1).Grade__c = 'C';
        evaluations.get(1).Result__c = '';
        evaluations.get(2).Grade__c = 'D';
        evaluations.get(2).Result__c = '';
        
        update evaluations;
        StudentsController sc = new StudentsController();
        sc.saveEvaluations();
        Map<Id, Participation__c> resultsAndRatio = new Map<Id, Participation__c>([SELECT Id, Result__c, Course__r.Succes_ratio__c  FROM Participation__c WHERE Id IN: evaluations]);    
        System.assertEquals(((double)2/3)*100, resultsAndRatio.get(evaluations.get(0).Id).Course__r.Succes_ratio__c);
        System.assertEquals('Pass', resultsAndRatio.get(evaluations.get(0).Id).Result__c);
        System.assertEquals('Pass', resultsAndRatio.get(evaluations.get(1).Id).Result__c);
        System.assertEquals('Fail', resultsAndRatio.get(evaluations.get(2).Id).Result__c);
        clearData();
    }
    
    @isTest 
    static void calculateResult2(){
        setup();
        evaluations = [SELECT Hours_attended__c, 
                                             Grade__c, 
                                             Result__c, 
                                             Student__r.Name,
                                             Student__r.last_Name__c, 
                                             Course__r.End_date__c, 
                                             Course__r.Duration__c, 
                                             Course__r.Name,
                                             Course__r.Succes_ratio__c
                                             FROM Participation__c
                                             WHERE Hours_attended__c >=: requiredHoursToBeEvaluated
                                             ];
        
        
        System.debug('evaluations' + evaluations);

		evaluations.get(0).Grade__c = 'F';
        //evaluations.get(0).Result__c = '';
        evaluations.get(1).Grade__c = 'A';
        //evaluations.get(1).Result__c = '';
        evaluations.get(2).Grade__c = 'B';
        //evaluations.get(2).Result__c = '';
        
    
        update evaluations;
        
        StudentsController sc = new StudentsController();
        sc.saveEvaluations();

        Map<Id, Participation__c> results = new Map<Id, Participation__c>([SELECT Id, Result__c FROM Participation__c WHERE Id IN: evaluations]);
        System.assertEquals('Fail', results.get(evaluations.get(0).Id).Result__c);
        System.assertEquals('Pass', results.get(evaluations.get(1).Id).Result__c);
        System.assertEquals('Pass', results.get(evaluations.get(2).Id).Result__c);
        clearData();
        
    }
    
    @isTest 
    static void testGetters(){
        setup();
        StudentsController sc = new StudentsController();
        
        System.assertNotEquals(null, sc.getItemsOfCourses());
        System.assertNotEquals(null, sc.getItemsOfGrades());
        System.assertNotEquals(null, sc.getCourses());
        System.assertNotEquals(null, sc.getGrades());      
        clearData();
    }
}