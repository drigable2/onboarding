@RestResource(urlMapping='/Contact/*')
global with sharing class TaskWebRest  {

    private static List<List<Id>> contactsAndAccountsIdsList;

    @HttpGet
    global static List<List<Id>> getIds() {
        //some text
        RestRequest request = RestContext.request;
        // grab the dateRequest from the end of the URL
        Date dateRequest = Date.parse(request.requestURI.substring(request.requestURI.lastIndexOf('/')+1));
        
        contactsAndAccountsIdsList = new List<List<Id>>{new List<Id>(), new List<Id>()};
        for(Account account : [SELECT Id FROM Account WHERE CreatedDate=:dateRequest]){
            contactsAndAccountsIdsList[0].add(account.Id);
        }

        for(Contact contact : [SELECT Id FROM Contact WHERE CreatedDate=:dateRequest]){
            contactsAndAccountsIdsList[0].add(contact.Id);
        }
        
        return contactsAndAccountsIdsList;

    }

    @HttpPost
    global static List<List<Id>> createAccountsAndContacts(String json) {

        List<List<SObject>> contactsAndAccountsList;
        try {
            contactsAndAccountsList = (List<List<SObject>>)System.JSON.deserialize(json, List<List<SObject>>.class);
        } catch (Exception e) {
           
        }
        
        try {
           insert contactsAndAccountsList[0];
        } catch (Exception e) {
           
        }

        try {
           insert contactsAndAccountsList[1];
        } catch (Exception e) {
           
        }
        
        contactsAndAccountsIdsList = new List<List<Id>>{new List<Id>(), new List<Id>()};
        
        for(Contact contact : (List<Contact>)contactsAndAccountsList.get(0)){
            contactsAndAccountsIdsList[0].add(contact.Id);
        }
        for(Account account : (List<Account>)contactsAndAccountsList.get(1)){
            contactsAndAccountsIdsList[1].add(account.Id);
        }
        
        return contactsAndAccountsIdsList;

    }

    @HttpPut
    global static Contact updateObject() {

        

        Contact obj;

        return obj;

    }

    @HttpDelete
    global static void remove() {

    }

}