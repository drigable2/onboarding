public class StudentsController {
    
    private List<Course__c> courses = new List<Course__c>();
    private Set<String> completedCourses = new Set<String>();
    public Map<String, List<Participation__c>> completedParticipations = new Map<String, List<Participation__c>>();
   	private List<Participation__c> toBeEvaluated = new List<Participation__c>();
    private List<Participation__c> insufficientAttendance = new List<Participation__c>(); 
    private List<Participation__c> graded = new List<Participation__c>(); 
    private final List<String> grades = new List<String> {'A', 'B', 'C', 'D', 'E', 'F'};
    private string selectedCourse;
 
    
    public StudentsController(){
        collectCourses();
  		collectCompletedParticipations(collectParticipations());
       // initEmptyEvaluations(selectedCourse);
        
        
    }

    public List<Course__c> getCourses(){
        return courses;
    }
    
    public List<String> getGrades(){
        return grades;
    }
       
    public String getSelectedCourse(){
        return selectedCourse;
    }
    
    public void setSelectedCourse(String selectedCourse){
        this.selectedCourse = selectedCourse;
    }
    
    public Map<String, List<Participation__c>> getCompletedParticipations(){
        return completedParticipations;
    }
    
    public void setCompletedParticipations(Map<String, List<Participation__c>> completedParticipations){
        this.completedParticipations = completedParticipations;
    }
    
    
    public List<Participation__c> getToBeEvaluated(){
        return toBeEvaluated;
    }
        
    public List<Participation__c> getInsufficientAttendance(){
        return insufficientAttendance;
    }
    
    public List<Participation__c> getGraded(){
        return graded;
    }
    
    private void collectCourses(){
        courses = [SELECT Name, 
                   		  Start_date__c, 
                   	      End_date__c, 
                   		  Enrolled_students__c, 
                   		  Succes_ratio__c, 
                   		  Duration__c 
                   FROM Course__c];
    }
    
    @TestVisible
    private void collectCompletedParticipations(List<Participation__c> completed){
        
        for(Participation__c participation : completed)
		       		completedCourses.add(participation.Course__r.Name);
        if(completedCourses != null && completedCourses.size() > 0){
            if(completed != null && completed.size() > 0)
       	 		selectedCourse = completed[0].Course__r.Name;
        for(String completedCourse : completedCourses){
            completedParticipations.put(completedCourse, new List<Participation__c>());
        }
        for(Participation__c completedParticipation : completed)
        	completedParticipations.get(completedParticipation.Course__r.Name).add(completedParticipation); 
        sortEvaluation(completedParticipations.get(selectedCourse));
        }
    }
    
    public List<Participation__c> collectParticipations(){
        
        List<Participation__c> completed = [SELECT Hours_attended__c,       
                             		Grade__c, 
                             		Result__c, 
                             		Student__r.Name,
                              		Student__r.last_Name__c, 
                             		Course__r.End_date__c, 
                             		Course__r.Duration__c, 
                             		Course__r.Name,
						      		Course__r.Succes_ratio__c
                              FROM Participation__c
                              WHERE Course__r.End_date__c <= :Date.today()];
        
        return completed;
         
    }
    
    @TestVisible public void sortEvaluation(List<Participation__c> evaluations){
		clearEvaluations(selectedCourse);        
        for(Participation__c e : evaluations){
            if(e.Hours_attended__c >= 
               System.Math.round(e.Course__r.Duration__c / 2) && e.Result__c == null)
                   toBeEvaluated.add(e);

            else if(e.Hours_attended__c < 
                    System.Math.round(e.Course__r.Duration__c / 2))
                        insufficientAttendance.add(e);        
                    
            else if(e.Result__c != null)
                graded.add(e);
            
        }
        
    }
    
    public void clearEvaluations(String courseName){
        toBeEvaluated.clear();
        insufficientAttendance.clear();
        graded.clear();
    }
             
    public List<SelectOption> getItemsOfGrades() {
        List<SelectOption> selectOptions = new List<SelectOption>();
        for(String grade: grades)
            selectOptions.add(new SelectOption(grade, grade));
        return selectOptions;
    }
    
    public List<SelectOption> getItemsOfCourses() {
        List<SelectOption> selectOptions = new List<SelectOption>();
        for(String courseName: completedCourses)
            selectOptions.add(new SelectOption(courseName, courseName));
        return selectOptions;
    }
    
    public PageReference selectCourse(){
        sortEvaluation(completedParticipations.get(selectedCourse));
        return null;
    }
    
    public PageReference selectGrade(){
        return null;
    }
    
    @TestVisible
    private void calculateResult(){
        for(Participation__c p: getToBeEvaluated()){
            if(p.Grade__c > 'C' && p.Grade__c != null)
                p.Result__c = 'Fail';
            else if(p.Grade__c <= 'C' && p.Grade__c != null)
                p.Result__c = 'Pass';
        }
    }

    public PageReference saveEvaluations() {
        if(!getToBeEvaluated().isEmpty()){
            
            for(Participation__c p: getInsufficientAttendance())
				p.Grade__c = 'F';
             
            List<Participation__c> participations = new  List<Participation__c>();                 
            
            calculateResult();
                
            update getToBeEvaluated();             
            update getInsufficientAttendance();
  
            participations.addAll(getToBeEvaluated());
            participations.addAll(getInsufficientAttendance());
            participations.addAll(getGraded());

            sortEvaluation(participations);
          return null;
            
        }else{
            if(!getInsufficientAttendance().isEmpty() && insufficientAttendance.get(0).Grade__c != 'F')
                for(Participation__c p: getInsufficientAttendance())
                p.Grade__c = 'F';
            return null;
        }
    }
  
}