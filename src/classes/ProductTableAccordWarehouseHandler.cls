public class ProductTableAccordWarehouseHandler {
	
    List<Warehouse__c> warehouses = [SELECT Id, Period_Start__c, Period_End__c, Name FROM Warehouse__c];
    Map<Product_Table__c, Warehouse__c> productInNewWarehouse = new Map<Product_Table__c, Warehouse__c>();
    
    public void insertWharehouse(){
        List<Product_Table__c> products = new List<Product_Table__c>(productInNewWarehouse.keySet());
        List<Warehouse__c> newWarehouses = new List<Warehouse__c>();
        for(Product_Table__c product : products)
            newWarehouses.add(productInNewWarehouse.get(product));
        insert newWarehouses;
        for(Product_Table__c product : products)
            product.Warehouse__c = productInNewWarehouse.get(product).Id;
        
    }
    
    public void defineWarehouse(List<Product_Table__c> productTables){
        
        for(Product_Table__c productTable : productTables){
            //productInNewWarehouse.put(productTable, productTable.Warehouse__r);
            for(Warehouse__c warehouse : warehouses){
                if(warehouse.Period_Start__c <= productTable.Added_Date__c &&
                   warehouse.Period_End__c >= productTable.Added_Date__c){
                       productTable.Warehouse__c = warehouse.Id;
                       break;
                } else {
                       Map<String, OrgConfiguration__c> conifg = OrgConfiguration__c.getAll();
                       Integer periodTerm = (conifg.containsKey(productTable.Name)) 
                          ? (Integer)conifg.get(productTable.Name).Period_Term__c 
                       	  : (Integer)conifg.get('Default').Period_Term__c;
                       Warehouse__c newWarehouse = new Warehouse__c();
                       
                       newWarehouse.Period_Start__c = productTable.Added_Date__c;
                       Date periodStart = productTable.Added_Date__c;
                       Date periodEnd = periodStart.addDays(periodTerm);
                       newWarehouse.Period_End__c = periodEnd;
                       newWarehouse.Name = 'Warehouse ' + periodStart + ' ' + periodEnd;
                       productInNewWarehouse.put(productTable, newWarehouse);
                   }
            }
            
        }
        
        insertWharehouse();
        
    }
             
}