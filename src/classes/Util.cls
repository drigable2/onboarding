public virtual class Util{

	public ApexPages.StandardSetController stdController {get; set;}
	private Static String productDetailUrl = (String) [SELECT DeveloperName, URL__c FROM Custom_URL__mdt 
											  WHERE DeveloperName='Product_Details' LIMIT 1].URL__c;
   private Static String purchaseFormUrl = (String) [SELECT DeveloperName, URL__c FROM Custom_URL__mdt 
											 WHERE DeveloperName='Purchase_Form' LIMIT 1].URL__c;

	public Util(Apexpages.StandardSetController stdController){
		this.stdController = stdController;
	}

	public Static String getProductDetailUrl(){
		return Util.productDetailUrl;
	}

	public Static String getPurchaseFormUrl(){
		return Util.purchaseFormUrl;
	}

}