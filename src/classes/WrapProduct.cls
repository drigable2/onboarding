public class WrapProduct {

	public Product__c product {get; set;}
   private Integer discountCost;
 
   public wrapProduct(Product__c product) {
		this.product = product;
   }

   public void setDiscountCost(Double discountCost){
      this.discountCost = Math.round(discountCost);
   }

   public Integer getDiscountCost(){
      return this.discountCost;
   }

}