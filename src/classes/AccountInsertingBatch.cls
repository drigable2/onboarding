global class AccountInsertingBatch implements Database.Batchable<SObject>, Database.Stateful {
	
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator('[select]');
    }
    
    global void execute(Database.BatchableContext bc, List<Account> accountsScope){
        
    }
    
    global void finish(Database.BatchableContext bc){
        
    }
    
}