public class WeatherCallouts {
    
    public static Map<String, External_Service_Access__c> config = External_Service_Access__c.getAll();

    public static HttpResponse makeGetCallout(String city) {
        
      	External_Service_Access__c access = config.get('Weather REST Service');
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(access.endPoint__c + '/forecast?q=' + city + '&APPID=' + access.appID__c);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        return response;

    }
    
}