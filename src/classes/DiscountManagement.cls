public class DiscountManagement {

	private  Map<Id, Discount__c> Ids2Discounts;
	private Discount__c minDiscount;
	private  Id matchedDiscountId;

	public Discount__c getMatchedDiscount(){
		return Ids2Discounts.get(this.matchedDiscountId);
	}

	public Discount__c getMinDiscount(){
		return this.minDiscount;
	}																											

	public DiscountManagement() {

		Ids2Discounts = new Map<Id, Discount__c>([SELECT Id, Discount_Code__c, 
																			  Percentes_of_discount__c, 
																			  Static_discount__c 
																FROM Discount__c]);

	}

	public Boolean checkDiscountAvailability(){

		if(UserInfo.getUserType() == 'Standard'){

			//ID contactId = [SELECT contactId FROM User WHERE Id =:Userinfo.getUserId()].contactId;
			//ID accountID  = [SELECT Id FROM Contact WHERE id =:contactId].AccountId;
			//Account account = [SELECT Id, (SELECT Discount__c FROM Discount_Account_Associations__r) FROM Account WHERE Id =:accountID LIMIT 1];
			//findMinDiscount(account);
			
		}else if(UserInfo.getUserType() == 'CspLitePortal'){
			ID contactId = [SELECT contactId FROM User WHERE Id =:Userinfo.getUserId()].contactId;
			ID accountID  = [SELECT accountId FROM Contact WHERE id =:contactId].accountId;
			Account account = [SELECT Id, 
										(SELECT Discount__r.Percentes_of_discount__c, Discount__r.Discount_Code__c, Discount__r.Static_discount__c 
									 	 FROM Discount_Account_Associations__r) 
									 FROM Account 
									 WHERE Id =:accountID LIMIT 1];
			if(account.Discount_Account_Associations__r != null && !account.Discount_Account_Associations__r.isEmpty()){
				findMinDiscount(account);
				return true;
			}
		}
		
		return false;

	}

	private void findMinDiscount(Account account){

		List<Discount__c> discounts = new List<Discount__c>();
		
		for(Discount_Account_Association__c daa : account.Discount_Account_Associations__r)
			discounts.add(daa.Discount__r);
		
		minDiscount = discounts[0];
		Double minCost = evalCostFrom(minDiscount, 1);

		for(Integer i = 1; i < discounts.size(); i++){
			Double comparedCost = evalCostFrom(discounts[i], 1);
			//minCost = Math.min(minCost, comparedCost);
			minDiscount = (minCost < comparedCost) ? minDiscount : discounts[i];							
		}
			
	}

	public Double evalCostFrom(Discount__c discount, Double productCost){

		Double costFromPercentes = productCost - productCost * (discount.Percentes_of_discount__c / 100);
		return (discount.Static_discount__c == null) ? costFromPercentes : costFromPercentes - discount.Static_discount__c;

	}

	public Boolean checkPormoCode(String promoCode){	
		
		for(Id discountId : Ids2Discounts.keySet()){
			if(Ids2Discounts.get(discountId).Discount_Code__c == promoCode){
				this.matchedDiscountId = discountId;
				return true;
			}	
		}
		return false;

	}

}