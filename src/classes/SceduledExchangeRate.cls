public  class SceduledExchangeRate implements Schedulable {
	public SceduledExchangeRate() {
		
	}

	public void execute(SchedulableContext ctx) {
		GetCurrentExchanges();
		ExchangeRateBatch erb = new ExchangeRateBatch(); 
		Id batchId = Database.executeBatch(erb);
    }

	 public void GetCurrentExchanges() {

        wwwGamaSystemComWebservices.ExchangeRatesSoap wgce = new wwwGamaSystemComWebservices.ExchangeRatesSoap();
        wgce.inputHttpHeaders_x = new Map<String, String>();
        wgce.inputHttpHeaders_x.put('Host', 'webservices.gama-system.com');
        wgce.inputHttpHeaders_x.put('Content-Type', 'text/xml; charset=utf-8');
        wgce.inputHttpHeaders_x.put('SOAPAction', 'http://www.gama-system.com/webservices/GetCurrentExchangeRate');
        
        Map<String, Double> rates = new Map<String, Double>();
        rates.put('USD2EUR', (Double)wgce.GetCurrentExchangeRate('BS', 'USD', 1));
        rates.put('JPY2EUR', (Double)wgce.GetCurrentExchangeRate('BS', 'JPY', 1));
        
		  Map<String, ExchangeRateConfiguration__c> config = ExchangeRateConfiguration__c.getAll();
		  config.get('USDtoEUR').RateValue__c = rates.get('USD2EUR');
		  config.get('JPYtoEUR').RateValue__c = rates.get('JPY2EUR');
		  update config.values();
		  
    }
}