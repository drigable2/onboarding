public virtual class RowMaterialTriggerHandler {
/* 
   public List<SObject> entities;
    public virtual List<Raw_Material__c> getEntities(){
        return (List<Raw_Material__c>)entities;
    }
    public List<Store__c> stores;
	Map<Raw_Material__c, Store__c> rawMaterialInStore = new Map<Raw_Material__c, Store__c>();
    List<Raw_Material__c> newEntities = new List<Raw_Material__c>(rawMaterialInStore.keySet());
	List<Store__c> newStores = new List<Store__c>();    
    //RawMaterial entitiy;
    
    public RowMaterialTriggerHandler(){
        
        entities = (new RawMaterial()).getEntities();
        defineStore((List<Raw_Material__c>)entities);
    }
    
    public void insertStore(){

        for(Integer i = 0; i < newEntities.size(); i++)
            newStores.add(rawMaterialInStore.get(newEntities[i]));
        insert newStores;
        for(Integer i = 0; i < newEntities.size(); i++)
           newEntities[i].Store__c = rawMaterialInStore.get(newEntities[i]).Id;
        
    }
   // public void executeDefintion(){
    //    List<Raw_Material__c> entities;
        
  //  }
  //  
    public static String testSobject(sObject objToProcess) {
        return (String)objToProcess.get('Name');
    }
    public virtual void defineStore(List<Raw_Material__c> entities){
        for(Integer i = 0; i < entities.size(); i++){
            
            for(Store__c store : stores){
                if(store.Start_Period__c <= entities[i].Date__c &&
                   store.End_Period__c >= entities[i].Date__c){
                       entities[i].Store__c = store.Id;
                       break;
                   } else {
                       Map<String, OrgConfiguration__c> conifg = OrgConfiguration__c.getAll();
                       Integer periodTerm = (conifg.containsKey(entities[i].Name)) 
                           ? (Integer)conifg.get(entities[i].Name).Period_Term__c 
                           : (Integer)conifg.get('Default').Period_Term__c;
                       Store__c newEntity = new Store__c();
                       
                       newEntity.Start_Period__c = entities[i].Date__c;
                       Date periodStart = entities[i].Date__c;
                       Date periodEnd = periodStart.addDays(periodTerm);
                       newEntity.End_Period__c = periodEnd;
                       newEntity.Name = 'Store ' + periodStart + ' ' + periodEnd;
                       rawMaterialInStore.put(entities[i], newEntity);
                   }
                
            }
            
            insertStore();
            
        }
      //  executeDefintion();
            
        
    }
    
    public virtual void defineStore(List<Product__c> entities){
        defineStore(entities);
    }

    class ProductTriggerHundler extends RowMaterialTriggerHandler {
        
    //public override List<Product__c> getEntities(){
        //return (List<Product__c>)entities;
    //}
        
        public ProductTriggerHundler(){
			entities = new Product().getEntities();            
        }

    }    */
}