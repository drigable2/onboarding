public class LeadTriggerBeforeHandler {
	public List<Lead> newLeads;
    public List<Campaign> parentCampaigns;
    private List<String> leadSources = new List<String>();
    
    public List<String> getLeadSources(){
        return leadSources;
    }
    
    public LeadTriggerBeforeHandler(List<Lead> newLeads){
        this.newLeads = newLeads;
        System.Debug('**********************************' + newLeads);
        for(Lead newLead : newLeads){
            leadSources.add(newLead.LeadSource);
        }
        System.Debug('#####################################' + leadSources);
    }
    
    public List<Campaign> getParentCampaignsWithoutChildren(){
        return Database.query('SELECT Name FROM Campaign WHERE Name IN :leadSources');
    }
}