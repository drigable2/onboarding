public class ArcherydbFieldMerger {
	
	public StaticResource resource1 = [SELECT Id, SystemModStamp, Body
															FROM StaticResource 
															WHERE Name = 'SageSCV'
															LIMIT 1];
	public StaticResource resource2 = [SELECT Id, SystemModStamp, Body
															FROM StaticResource 
															WHERE Name = 'Book1CSV'
															LIMIT 1];
	public Integer fieldNumber;
	public String fieldValue;
	public Map <String, Integer> fieldNumberMap = new Map <String, Integer>();
	public List<String> lstFieldNames = new List<String>();
	
	public List<Map<String, String>> parseSCV(StaticResource scvFile){

		List<Map<String, String>> records = new List<Map<String, String>>();
		String sageSCV = scvFile.Body.toString();
		List<String> scvDataLines = sageSCV.split('\n');
      List<String> csvFieldNames = scvDataLines[0].split(',');

		for (Integer i = 0; i < csvFieldNames.size(); i++) {
			fieldNumberMap.put(csvFieldNames[i], i);
			lstFieldNames.add(csvFieldNames[i].trim());
		}

		for (Integer i = 1; i < scvDataLines.size(); i++) {

         Map<String, String> recordMap = new Map<String, String>();   
			List<String> csvRecordData = scvDataLines[i].split(',');
			for (String fieldName: csvFieldNames) {
					fieldNumber = fieldNumberMap.get(fieldName);
					fieldValue = csvRecordData[fieldNumber];
					recordMap.put(fieldName.trim(), fieldValue.trim());
			}
			records.add(recordMap);
			               
		}
		return records;

	}

	public List<SageTask__c> replace2to1(){

		List<SageTask__c> sts = new List<SageTask__c>();
		List<Map<String, String>> records1 = parseSCV(resource1);
		List<Map<String, String>> records2 = parseSCV(resource2);
		List<String> fieldValues = new List<String>();
		
		for(Integer i = 0; i < records1.size(); i++){
			String deptNumber = records1[i].get('Dept');
			for(Integer j = 0; j < records2.size(); j++){
				if(records2[j].get('id') == deptNumber){
					sts.add(new SageTask__c(FieldReplaser__c = records2[j].get('departmenName')));
				}
			}
		}

		List<SageTask__c> oldValues = [SELECT id FROM SageTask__c];
		if(oldValues != null)
			delete oldValues;

		insert sts; 
		return sts;
		
	}

}