public class BuyingFormController {
    public Product__c product;
    Id productId = '';
    public Contact__c contact = new Contact__c();
    PageReference comletedBuying = new PageReference('https://c.ap5.visual.force.com/apex/successfulPurchase?core.apexpages.request.devconsole=1' + productId);
    PageReference errorBuying = new PageReference('https://c.ap5.visual.force.com/apex/failureOfPurchase?core.apexpages.request.devconsole=1' + productId);
     
    public Product__c getProduct(){
        return product;
    }
        
    public Contact__c getContact(){
        return contact;
    }
    
    public void setContact(Contact__c contact){
        this.contact = contact;
    }
    
    public BuyingFormController(){
        
        productId += ApexPages.currentPage().getParameters().get('id');
        product = [SELECT Name, Title__c, Image__c, Description__c, Cost__c, Amount__c 
                    FROM Product__c WHERE Id =:productId LIMIT 1];
    }
        
    public PageReference buyProduct(){
        
        PurchaseProcess bp = new PurchaseProcess(); 
        PageReference reference = (bp.getPurchaseStatus(bp.buy(product, contact))) ? comletedBuying : errorBuying;
        reference.setRedirect(true);
        return reference;
        
    }
}