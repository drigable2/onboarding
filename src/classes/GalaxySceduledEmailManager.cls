public class GalaxySceduledEmailManager implements Schedulable {
	
	public void execute(SchedulableContext ctx){
		
    Id profileId = [SELECT Id FROM Profile WHERE Name ='System Administrator'].Id;
    List<String> toAddresses = new List<String>();

    User admin = [SELECT Email FROM User WHERE ProfileId =:profileId LIMIT 1];
    
    toAddresses.add(admin.Email);

    List<Messaging.SingleEmailMessage> listSingleEmailMessage = new List<Messaging.SingleEmailMessage>();
    Id emailTemplateId = [SELECT id FROM EmailTemplate WHERE Name='New_Customer_Email_Alert'].Id;
    Id customerProfileId = [SELECT Id FROM Profile WHERE Name ='Customers'].Id;

    CronTrigger cronTrigger = [SELECT PreviousFireTime, StartTime FROM CronTrigger WHERE Id=:ctx.getTriggerId()];
    DateTime initTime = cronTrigger.StartTime;
    initTime.addHours(-6);

    List<User> newUsers;
    if(cronTrigger.PreviousFireTime != null)
      newUsers = [SELECT ContactId FROM User WHERE ProfileId =:customerProfileId AND CreatedDate >= :cronTrigger.PreviousFireTime];
    else 
      newUsers = [SELECT ContactId FROM User WHERE ProfileId =:customerProfileId AND CreatedDate >= :initTime]; 
    
    if(newUsers != null & !newUsers.isEmpty()){
      for (User newUser : newUsers){
        Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(emailTemplateId, admin.Id, newUser.ContactId);
        listSingleEmailMessage.add(email);    
      }
    
      Messaging.SendEmailResult[] result = Messaging.sendEmail(listSingleEmailMessage);

    }

  }		

}