public class WeatherData {
        
    public List<Result> results{get;set;}
    
    
    class Result {
        public Map<String, Object> address_components{get;set;}
        public String formatted_address{get;set;}
        public Geometry geometry{get;set;}
        public String place_id{get;set;}
        public List<String> types{get;set;}
        
    }
    
    class Geometry {
        public Object bounds{get;set;}
        public Map<String, String> location{get;set;}
        public Object viewport{get;set;}
    }
    
}