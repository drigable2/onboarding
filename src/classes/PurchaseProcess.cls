public class PurchaseProcess {
    
	private List<Contact__c> contact;
    private ID contactId = [SELECT contactId FROM User WHERE Id =:Userinfo.getUserId()].contactId;
    public List<Contact__c> selectContact(String email){
        return new List<Contact__c>([SELECT Name, E_Mail__c, Adress__c, Phone__c 
                    FROM Contact__c WHERE E_Mail__c =:email LIMIT 1]);
    }
   
    private Boolean checkProductAbsence(Product__c product){
        Integer amount = (Integer)[SELECT Amount__c FROM Product__c WHERE Id =:product.Id].Amount__c;
        return (amount == 0) ? true : false;
    }
    
    private void insertSoldProduct(Product__c product){
        
        Product__c soldProduct = product.clone(false, true);
        RecordType soldRecordType = [SELECT Id FROM RecordType WHERE Name = 'Sold' and SobjectType = 'Product__c'];
        soldProduct.RecordTypeId = soldRecordType.Id;
        //soldProduct.Contact__c = contact.get(0).Id;
        
        soldProduct.Contact__c = contactId;
        insert soldProduct;
        Id emailTemplateId = [SELECT id FROM EmailTemplate WHERE Name='Purchase_Product_Email_Alert'].Id;
        Messaging.SingleEmailMessage email = Messaging.renderStoredEmailTemplate(emailTemplateId, contactId, soldProduct.Id);
        System.debug('soldRecordType -------------------' + soldRecordType);
        
    }
    
    public Boolean getPurchaseStatus(Boolean status){
        return status;
    }
    
    public Boolean buy(Product__c product, Contact__c contactFormData){

        if(checkProductAbsence(product)){
            return false;
        }
        
        contact = selectContact((String)contactFormData.E_Mail__c);
        if(!contact.isEmpty()){
            
            if(contactFormData.Name != contact.get(0).Name)
                contact.get(0).Name = contactFormData.Name;
            else if(contactFormData.Adress__c != contact.get(0).Adress__c)
                contact.get(0).Adress__c = contactFormData.Adress__c;
            else if(contactFormData.Phone__c != contact.get(0).Phone__c)
                contact.get(0).Phone__c = contactFormData.Phone__c;
 
            update contact;
            
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Such a contact does not exist'));
            //contact.add(contactFormData);
            //insert contact;   
        }
        
        insertSoldProduct(product);

        

        product.Amount__c += -1;
        update product; 
        
        return true;
        
    }
    
}