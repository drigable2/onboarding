public class CampaignUpdateAction {
    
    @InvocableMethod(label='Update Campaigns' description='Updates the campaigns specified and upadtes relaited contacts')
    public static void updateCampaigns(List<Campaign> campaigns) {
        List<Id> compaignsIds = new List<Id>();
        for(Campaign campaign : campaigns){
            compaignsIds.add(campaign.Id);
        }
        List<Campaign> campaignsWithContacts = [SELECT Name, (SELECT Contact.Description FROM CampaignMembers) FROM Campaign WHERE Id IN:compaignsIds];
        Set<Contact> contactInAllModifiedCompaigns = new Set<Contact>();
        
        for(Campaign campaign : campaignsWithContacts){
            List<CampaignMember> campaignMembers = campaign.CampaignMembers;
            for(CampaignMember campaignMember : campaignMembers){
            	campaignMember.Contact.Description = 'The campaign name with the associated contact is "' + campaign.Name + '"';
                contactInAllModifiedCompaigns.add(campaignMember.Contact);
            }
        }
        
        update new List<Contact>(contactInAllModifiedCompaigns);
    }
    
}