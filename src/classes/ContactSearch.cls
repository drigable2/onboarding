public class ContactSearch {
    public static List<Contact> searchForContacts(String lastName, String postalCode){
        String validLastName = '%' + lastName;
        List<Contact> contacts = [SELECT Name, MailingPostalCode
                                  FROM Contact
                                  WHERE Name LIKE :validLastName AND MailingPostalCode =:postalCode];
        return contacts;
    }
}