@isTest
public class CampaignUpdateActionTest {
    
    @testSetup 	
    static void setup(){
        
        Campaign campaign = new Campaign(Name = 'Test Campaign');
        Contact contact = new Contact(FirstName = 'John',
                                      LastName = 'Snow',
                                      Description = '');
        insert contact;
        insert campaign;
        CampaignMember campaignMember = new CampaignMember();
        campaignMember.CampaignId = campaign.Id;
        campaignMember.ContactId = contact.Id;
        insert campaignMember;
        
    }
    
    @isTest
    static void TestUpdateCampaigns(){
      	
        List<Campaign> campaigns = [SELECT Id FROM Campaign];
        
        CampaignUpdateAction.updateCampaigns(campaigns);
        
        List<Contact> contacts = [SELECT Description FROM Contact WHERE Name = 'John Snow'];
        System.debug(campaigns);
        System.assertEquals(true, campaigns != null);
        System.assertEquals(true, !campaigns.isEmpty());
        System.assertEquals(true, contacts.get(0).Description == 'The campaign name with the associated contact is "Test Campaign"');
    
    }
	
}