@isTest
public class ProductTableAccordWarehouseTest {
	
    
    private static List<Product_Table__c> setup(){
        
        OrgConfiguration__c conifg_1 = new OrgConfiguration__c();
        conifg_1.Period_Term__c = 365;
        conifg_1.Name = 'Pen';
        
        OrgConfiguration__c conifg_2 = new OrgConfiguration__c();
        conifg_2.Period_Term__c = 12;
        conifg_2.Name = 'Default';
        
        OrgConfiguration__c conifg_3 = new OrgConfiguration__c();
        conifg_3.Period_Term__c = 5;
        conifg_3.Name = 'Egg';
        
        insert conifg_1;
        insert conifg_2;
        insert conifg_3;
        
        Warehouse__c warehouseDefault = new Warehouse__c();
        warehouseDefault.Name = 'Default';
        warehouseDefault.Period_Start__c = Date.newInstance(2018, 6, 12);
        warehouseDefault.Period_End__c = Date.newInstance(2018, 6, 14);
        
        Product_Table__c product_1 = new Product_Table__c();
        product_1.Name = 'Pen';
        product_1.Added_Date__c = Date.newInstance(2018, 6, 18);
        
        Product_Table__c product_2 = new Product_Table__c();
        product_2.Name = 'Egg';
        product_2.Added_Date__c = Date.newInstance(2018, 6, 20);
        
        Product_Table__c product_3 = new Product_Table__c();
        product_3.Name = 'Strawberry';
        product_3.Added_Date__c = Date.newInstance(2020, 7, 15);
        
        insert warehouseDefault;
        
        List<Product_Table__c> products = new List<Product_Table__c>();
        products.add(product_1);
        products.add(product_2);
        products.add(product_3);
       
        insert products;
        return products;
    }
    
    private static void clearData() {
        delete [SELECT Id FROM Warehouse__c];
    }
    
    @isTest
    static void testDefineWarehouse_1(){
      	List<Product_Table__c> products = setup();    
        Map<Id, Product_Table__c> product = new Map<Id, Product_Table__c>([SELECT Id, Name, Warehouse__r.Period_Start__c, Warehouse__r.Period_End__c, 
                                  Warehouse__r.Name 
                                  FROM Product_Table__c
                  					WHERE Warehouse__r.Name = 'Warehouse 2018-06-18 00:00:00 2019-06-18 00:00:00']);
        System.debug(products);
        System.assertEquals(true, product != null);
        System.assertEquals(true, !product.isEmpty());
        System.assertEquals(true, product.get(products.get(0).Id).Name == 'Pen');
        //clearData();     
    }
    
    @isTest
    static void testDefineWarehouse_2(){
      	List<Product_Table__c> products = setup();    
        Map<Id, Product_Table__c> product = new Map<Id, Product_Table__c>([SELECT Id, Name, Warehouse__r.Period_Start__c, Warehouse__r.Period_End__c, 
                                  Warehouse__r.Name 
                                  FROM Product_Table__c
                  					WHERE Warehouse__r.Name = 'Warehouse 2018-06-20 00:00:00 2018-06-25 00:00:00']);
        System.debug(products);
        System.assertEquals(true, product != null);
        System.assertEquals(true, !product.isEmpty());
        System.assertEquals(true, product.get(products.get(1).Id).Name == 'Egg');
        //clearData();     
    }
    
    @isTest
    static void testDefineWarehouse_3(){
      	List<Product_Table__c> products = setup();    
        Map<Id, Product_Table__c> product = new Map<Id, Product_Table__c>([SELECT Id, Name, Warehouse__r.Period_Start__c, Warehouse__r.Period_End__c, 
                                  Warehouse__r.Name 
                                  FROM Product_Table__c
                  					WHERE Warehouse__r.Name = 'Warehouse 2020-07-15 00:00:00 2020-07-27 00:00:00']);
        System.debug(products);
        System.assertEquals(true, product != null);
        System.assertEquals(true, !product.isEmpty());
        System.assertEquals(true, product.get(products.get(2).Id).Name == 'Strawberry');
        //clearData();     
    }
    @isTest
    static void testDefineWarehouse_4(){
      	List<Product_Table__c> products = setup();
        
        /*Product_Table__c product_1 = new Product_Table__c();
        product_1.Name = 'Pen';
        product_1.Added_Date__c = Date.newInstance(2018, 6, 18);
        insert product_1;
        
        Product_Table__c newProduct = new Product_Table__c();
        newProduct.Name = 'Egg';
        newProduct.Added_Date__c = Date.newInstance(2018, 6, 19);
        insert newProduct;*/
        
        Map<Id, Product_Table__c> product = new Map<Id, Product_Table__c>([SELECT Id, Name, Warehouse__r.Period_Start__c, Warehouse__r.Period_End__c, 
                                  Warehouse__r.Name 
                                  FROM Product_Table__c
                  					WHERE Warehouse__r.Name = 'Warehouse 2018-06-18 00:00:00 2019-06-18 00:00:00']);
        
        
        
        System.debug(products);
        System.assertEquals(true, product != null);
        System.assertEquals(true, !product.isEmpty());
        System.assertEquals(true, product.keySet().size() == 1);
        System.assertEquals(true, product.get(products.get(1).Id).Name == 'Egg');
        //clearData();     
    } 
    
}