public with sharing class ProductExtendedController {

    
    private List<WrapProduct> wrapProductList = new List<wrapProduct>(); 
	private final Product__c product;
    private Boolean discountAvailability = false;
    
    private  String productDetailUrl = (String) [SELECT DeveloperName, URL__c FROM Custom_URL__mdt 
											  WHERE DeveloperName='Product_Details' LIMIT 1].URL__c;
    private  String purchaseFormUrl = (String) [SELECT DeveloperName, URL__c FROM Custom_URL__mdt 
											 WHERE DeveloperName='Purchase_Form' LIMIT 1].URL__c;

    public ProductExtendedController(ApexPages.StandardController stdController) {

        
        list<String> MyFieldList;
        MyFieldList = new list<String>{'Id', 'Name', 'Amount__c', 'Title__c', 'Description__c', 'Image__c'};
        stdController.addFields(MyFieldList);
        this.product = (Product__c)stdController.getRecord();

        RecordType recordType = [SELECT Id, Name FROM RecordType WHERE Name = 'Made' and SobjectType = 'Product__c' LIMIT 1];
        
        for(Product__c product : [SELECT Name, Title__c, Image__c, Description__c, Cost__c, Amount__c 
                                                             FROM Product__c 
                                                             WHERE RecordTypeId=:recordType.Id]){
            wrapProductList.add(new WrapProduct(product));
        }

        DiscountManagement discountManagement = new DiscountManagement();
        if(UserInfo.getUserType() != 'Guest')
            discountAvailability = discountManagement.checkDiscountAvailability();
        

        if(discountAvailability){
            for(WrapProduct wp : wrapProductList){
                wp.setDiscountCost(discountManagement.evalCostFrom(discountManagement.getMinDiscount(), (Double)wp.product.Cost__c));
            }
        }
        

    }

    public Boolean getDiscountAvailability(){
        return this.discountAvailability;
    }

    public List<WrapProduct> getWrapProductList(){
        return wrapProductList;
    }

    public Product__c getProduct(){
        return this.product;
    }



    public String getProductDetailUrl(){
		return this.productDetailUrl;
	}

	public String getPurchaseFormUrl(){
		return this.purchaseFormUrl;
	}
    
}