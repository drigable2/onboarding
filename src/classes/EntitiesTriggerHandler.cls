public class EntitiesTriggerHandler {
    
    List<SObject> allNewEntities = new List<SObject>();
    public List<Date> addedDates;
    public List<Store__c> suitableStores;
    public Set<Store__c> newStores = new Set<Store__c>();
    Map<String, Store_Configuration__c> conifg;
    
    public EntitiesTriggerHandler(List<SObject> newEntities){
        
        getSortedDateFromMinToNax(newEntities);
        
        String queryPart = '';
        Integer count = 0;
        Date d;
        for(Date addedDate : addedDates){
            d = addedDate;
            queryPart += '(Start_Period__c <= '+ addedDate + ' AND End_Period__c >= ' + addedDate + ')';
            if(count != addedDates.size() - 1)
                queryPart += ' OR';
            count++;
        }
        System.Debug('queryPart ----------------------------' + queryPart);
        System.Debug('addedDates ---------------------------' + addedDates);
        //ORDER BY Start_Period__c
        //suitableStores = Database.query('SELECT Start_Period__c, End_Period__c, Name FROM Store__c WHERE ' + queryPart);
        suitableStores = Database.query('SELECT Start_Period__c, End_Period__c, Name FROM Store__c WHERE Start_Period__c <= :d AND End_Period__c>=:d');
        conifg = Store_Configuration__c.getAll();
        System.Debug('suitableStores *****************************************' + suitableStores);
        assignEntityToStores(newEntities);
        
        
    }
    
    public void getSortedDateFromMinToNax(List<SObject> newEnities){
        addedDates = new List<Date>();
        for(SObject newEntity : newEnities){
            addedDates.add((Date)newEntity.get('Date__c'));
        }
        addedDates.sort();
    }
    
    public void createNewStoreForNewEntity(SObject newEntity){
        if(newStores.size() != 0){
            for(Store__c newStore : newStores){
                if((Date)newEntity.get('Date__c') == newStore.Start_Period__c){
                    
                    return;
                }
                    
            }
        }
        Integer periodTerm = (Integer)conifg.get('Default').Period_Term__c;
        Store__c newStore = new Store__c();
        Date startPeriod = (Date)newEntity.get('Date__c');
        newStore.Start_Period__c = startPeriod;
        Date endPeriod = startPeriod.addDays(periodTerm);
        newStore.End_Period__c = endPeriod;
        Store__c StoreWithNewEntity = new Store__c();
        newStores.add(newStore);
        allNewEntities.add(newEntity);
        
        System.debug('newStores %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%' + this.newStores);
        System.debug('newStores %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%' + this.newStores);

    }
    
    public void assignEntityToStores(List<SObject> newEntities){
        if(!suitableStores.isEmpty()){
            for(SObject newEntity : newEntities){
                
                for(Store__c store : suitableStores){
                    Integer count = 0;
                    
                    if((Date)newEntity.get('Date__c') >= store.Start_Period__c && (Date)newEntity.get('Date__c') <= store.End_Period__c){
                        newEntity.put('Store__c', Store.Id);
                        allNewEntities.add(newEntity);
                        break;
                    }
                    
                    if(count == suitableStores.size() - 1){
                        createNewStoreForNewEntity(newEntity);
                    }
                    count++;
                }
                    
            }
        } else {
            for(SObject newEntity : newEntities){
                createNewStoreForNewEntity(newEntity);
            }
        }

        // Insert a new stores in the custom Store__c
        System.Debug('newStores ##################################################' + newStores);
        insert new List<Store__c>(newStores);

        System.Debug('newStores ##################################################' + newStores);
        
        
        for(Store__c newStoreId : newStores){
            for(SObject newEntityObj : allNewEntities){
                if(newEntityObj.get('Date__c') == newStoreId.Start_Period__c){
        				System.Debug('enitityObj eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee' + newEntityObj);                
                        newEntityObj.put('Store__c', newStoreId.Id);                
                    break;
                }
                	
            }
        }
        
        
           
    }
    /*
    Map<String, OrgConfiguration__c> conifg = OrgConfiguration__c.getAll();
    Integer periodTerm = (conifg.containsKey(entities[i].Name)) 
    ? (Integer)conifg.get(entities[i].Name).Period_Term__c 
    : (Integer)conifg.get('Default').Period_Term__c;
 */
      
}