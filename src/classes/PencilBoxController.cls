public class PencilBoxController {
    
    public Pencil_Box__c pencilBox = [SELECT Name, 
                             (
                                 SELECT  Name, CreatedDate, OwnerId, Pencil_Box__c
                                 FROM pencils__r
                             )
                             FROM Pencil_Box__c LIMIT 1];
    
    public Pencil_Box__c getPencilBox(){
        return pencilBox;
    }

    public void setPencilBox(Pencil_Box__c pencilBox){
        this.pencilBox = pencilBox;
    }
    
	public PageReference quicksave(){
        try {
                update pencilBox;
            } catch (DmlException e) {
                System.debug(e.getMessage());
            }
        return null;
    }
}