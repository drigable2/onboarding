public class PurchaseFormController {
    
	public WrapProduct wrapProduct;
    transient Id productId;
    String succesUrl = '/apex/successfulPurchase?core.apexpages.request.devconsole=1&id=';
    String errorUrl = '/apex/failureOfPurchase?core.apexpages.request.devconsole=1&id=';
    public Contact__c contact = new Contact__c();
    private DiscountManagement discountManagement;
    public Discount__c inputDiscountField = new Discount__c();
    

    public WrapProduct getWrapProduct(){
        return wrapProduct;
    }

    public void setWrapProduct(WrapProduct wrapProduct){
        this.wrapProduct = wrapProduct;
    }
        
    public Contact__c getContact(){
        return contact;
    }

    public void setContact(Contact__c contact){
        this.contact = contact;
    }

    public Discount__c getInputDiscountField(){
        return inputDiscountField;
    }

    public void setInputDiscountField(Discount__c inputDiscountField){
        this.inputDiscountField = inputDiscountField;
    }
    
    public PurchaseFormController(){
        
        productId = ApexPages.currentPage().getParameters().get('id');
                       
        wrapProduct = new WrapProduct([SELECT Name, Title__c, Image__c, Description__c, Cost__c, Amount__c, Store__c
                                      FROM Product__c 
                                      WHERE Id =:productId 
                                      LIMIT 1]);

        discountManagement = new DiscountManagement();
        
    }
    public Pagereference applyPromoCode(){
        
        if(discountManagement.checkPormoCode(inputDiscountField.Discount_Code__c)){
            wrapProduct.setDiscountCost(discountManagement.evalCostFrom(discountManagement.getMatchedDiscount(), (Double)wrapProduct.product.Cost__c));
        }
        else {
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Such a promotional code does not exist'));
           
        }
        return null;

    }
 
    public PageReference buyProduct(){
        
        PurchaseProcess pp = new PurchaseProcess(); 
        PageReference reference = (pp.getPurchaseStatus(pp.buy(wrapProduct.product, contact))) ? new PageReference(succesUrl + productId) 
            																	               : new PageReference(errorUrl + productId);
        reference.setRedirect(true);
        return reference;
        
    }
    
}