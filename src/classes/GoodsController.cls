public class GoodsController extends ProductExtendedController{

    private List<Product__c> products;
    
    public GoodsController(){

        RecordType recordType = [SELECT Id, Name FROM RecordType WHERE Name = 'Made' and SobjectType = 'Product__c' LIMIT 1];
        
        products = [SELECT Name, Title__c, Image__c, Description__c, Cost__c, Amount__c 
                    FROM Product__c WHERE RecordTypeId=:recordType.Id];

    }

    public List<Product__c> getProducts(){
        return products;
    }
    
}