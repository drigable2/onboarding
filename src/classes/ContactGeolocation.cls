public class ContactGeolocation {
    
    public static Map<Id, Contact> idsAndContats = new Map<Id, Contact>();
    public static Map<String, External_Service_Access__c> config = External_Service_Access__c.getAll();
    
    
    @future(callout=true)
    public static void updateContactGeolocation(){
        System.debug('hello');
        List<Contact> contacts = [SELECT MailingAddress, Geo__c 
                                  FROM Contact 
                                  WHERE MailingStreet  != null And MailingCity != null];
        
            for(Contact contact : contacts)
                idsAndContats.put(contact.Id, contact);
                
            for(Id contactId : idsAndContats.keySet()){
                
            Address address = idsAndContats.get(contactId).MailingAddress;
            HttpResponse response = makeGetCallout(address);
            
            if (response.getStatusCode() == 200) {
                
                JSONParser parser = JSON.createParser(response.getBody());
                while (parser.nextToken() != null) {
                    
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'location')) {
                        parser.nextToken();
                        parser.nextToken();
                        parser.nextToken();
                        
                        String lat = parser.getText();
                        
                        parser.nextToken();
                        parser.nextToken();
                        
                        String lng = parser.getText();
                        
                        idsAndContats.get(contactId).Geo__Latitude__s = Double.valueOf(lat);
                        idsAndContats.get(contactId).Geo__Longitude__s = Double.valueOf(lng);
                        
                        break;
                    }
                    
                }
                
            }
            
        }
        
        update idsAndContats.values();
        
    }
    
    public static HttpResponse makeGetCallout(Address address) {
        
        External_Service_Access__c access = config.get('Geolocation Rest API');
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        String url = (access.endPoint__c + '/json?address=' + address.getStreet() + '&Key=' + access.appID__c).replaceAll('\\s+', '%20');
        
        request.setEndpoint(url);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        return response;
        
    }    
    
}