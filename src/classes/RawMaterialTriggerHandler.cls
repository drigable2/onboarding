public class RawMaterialTriggerHandler {
	
    List<Store__c> stores = [SELECT Start_Period__c, End_Period__c, Name FROM Store__c];
    Map<Raw_Material__c, Store__c> rawMaterialInStore = new Map<Raw_Material__c, Store__c>();
    Map<Product_Table__c, Warehouse__c> productInNewWarehouse = new Map<Product_Table__c, Warehouse__c>();
    
    
    public void insertStore(){
        
        
        
        List<Raw_Material__c> materials = new List<Raw_Material__c>(rawMaterialInStore.keySet());
        List<Store__c> newStores = new List<Store__c>();
        for(Raw_Material__c material : materials)
            newStores.add(rawMaterialInStore.get(material));
        insert newStores;
        for(Raw_Material__c material : materials)
            material.Store__c = rawMaterialInStore.get(material).Id;
        
    }
    
    public void defineWarehouse(List<Raw_Material__c> materials){
        
      /*  for(Raw_Material__c material : materials){
            
            for(Store__c store : stores){
                if(store.Start_Period__c <= material.Date__c &&
                   store.End_Period__c >= material.Date__c){
                       material.Store__c = store.Id;
                       break;
                } else {
                       Map<String, OrgConfiguration__c> conifg = OrgConfiguration__c.getAll();
                  //     Integer periodTerm = (conifg.containsKey(productTable.Name)) 
                  //        ? (Integer)conifg.get(productTable.Name).Period_Term__c 
                       	  : (Integer)conifg.get('Default').Period_Term__c;
                       Store__c newStore = new Store__c();
                       
                       newStore.Start_Period__c = material.Date__c;
                       Date periodStart = material.Date__c;
                       Date periodEnd = periodStart.addDays(periodTerm);
                       newStore.End_Period__c = periodEnd;
                       newStore.Name = 'Warehouse ' + periodStart + ' ' + periodEnd;
                       rawMaterialInStore.put(material, newStore);
                   }
            }
            
        }
        */
        insertStore();
        
    }
}