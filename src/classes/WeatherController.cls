public class WeatherController {
    
    public List<WeatherData> weatherDatas = new List<WeatherData>();
    public String city = 'London';
    public List<String> mainKeys; 
    
    public WeatherController(){
        weatherRequest(this.city);
    }
    
    public List<WeatherData> getWeatherDatas(){
        return this.weatherDatas;
    }
    
    public List<String> getMainKeys(){
        return new List<String>(weatherDatas.get(0).main.keyset());
    }
    
    public void setCity(String city){
        this.city = city;
    }
    
    public String getCity(){
        return this.city.toLowerCase().capitalize();
    }
    
    public void weatherRequest(String city){  
        
        HttpResponse response = WeatherCallouts.makeGetCallout(city);
        
        if (response.getStatusCode() == 200) {
            JSONParser parser = JSON.createParser(response.getBody());
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'list')) {
                    parser.nextToken();
                    break;
                }
            }
            
            weatherDatas = (List<WeatherData>)parser.readValueAs(List<WeatherData>.class);
            
            String fieldName = parser.getCurrentName();
            String fieldValue = parser.getText();
            System.debug(fieldName + fieldValue);
            
            System.debug(weatherDatas);
            
            
        } else {
            this.city = 'Nothing found';
            weatherDatas = null;
        }
        
    }
    
    public PageReference viewDates(){
        weatherRequest(city);
        return null;
    }
    
}