public class LeadTriggerHandler {
    
    public List<Lead> newLeads;
    public List<Campaign> parentCampaigns;
    
    public List<String> leadSources = new List<String>();
    public List<String> getLeadSources(){
        return leadSources;
    }
    
    public LeadTriggerHandler(List<Lead> newLeads){
        this.newLeads = newLeads;
        for(Lead newLead : newLeads){
            leadSources.add(newLead.LeadSource);
        }
        
    }
    
    public List<Campaign> getParentCampaignsWithoutChildren(){
        return Database.query('SELECT Name FROM Campaign WHERE Name IN :leadSources');
    }
    
    public void assignLeadToCampaign(){
        
        String queryPart = '';
        System.Debug(newLeads[0].get('CreatedDate'));
        Date createdDate;
        for(Integer i = 0; i < newLeads.size(); i++){
            createdDate = ((DateTime)newLeads.get(i).get('CreatedDate')).Date();
            queryPart += '(StartDate <= :createdDate AND EndDate >= :createdDate)';
            if(i != newLeads.size() - 1)
                queryPart += ' OR';
            
        }
        parentCampaigns = Database.query('SELECT Name, (SELECT Name, StartDate, EndDate FROM ChildCampaigns WHERE ' + queryPart + ') FROM Campaign WHERE Name IN :leadSources');
        
        for(Lead lead : newLeads){
            for(Campaign campaign : parentCampaigns){
                if(lead.LeadSource == campaign.Name){
                    lead.Campaign__c = campaign.getSobjects('ChildCampaigns').get(0).Id;
                }
            }
            
        }
        update newLeads;
    }
    

        
    /*
    Campaign parentCampaign = [SELECT Name From Campaign WHERE Name = 'Twitter'];
	List<Campaign> parentCampaigns = new List<Campaign>();
	for(Integer i = 1; i <= 12; i++){
        Campaign campaign = new Campaign();
        campaign.Name = 'Twitter_0' + i + '_2018';
        Date startDate = Date.newInstance(2018, i, 1);
        campaign.StartDate = startDate;
        campaign.EndDate = startDate.addMonths(1).addDays(-1);
        campaign.ParentId = parentCampaign.Id;
        parentCampaigns.add(campaign);
	}
	insert parentCampaigns;
	*/
}