public class ExchangeRateBatch implements Database.Batchable<sObject>, Database.Stateful{

    Map<String, ExchangeRateConfiguration__c> config = ExchangeRateConfiguration__c.getAll();
    public Integer recordsProcessed = 0;
    public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator('[SELECT Salary__c, Currency_Exchange_Rate__c FROM Contact]');
	}

   	public void execute(Database.BatchableContext BC, List<Contact> contacts) {
           
           for(Contact contact: contacts){
               if(contact.Currency_Exchange_Rate__c == 'USD/EUR'){
                   contact.Salary__c *= config.get('USDtoEUR').RateValue__c;
               }else if(contact.Currency_Exchange_Rate__c == 'JPY/EUR'){
                   contact.Salary__c *= config.get('JPYtoEUR').RateValue__c;
               }else if(contact.Currency_Exchange_Rate__c == 'RUB/EUR'){
                   
               }else if(contact.Currency_Exchange_Rate__c == 'BYN/EUR'){

               }else{

               }
               recordsProcessed++;
           }

           update contacts;

	}
	
	public void finish(Database.BatchableContext BC) {
		System.debug(recordsProcessed + ' records processed. Shazam!');
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, 
            JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob
            WHERE Id = :bc.getJobId()];
	}
    
}